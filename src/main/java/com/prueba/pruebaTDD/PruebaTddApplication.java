package com.prueba.pruebaTDD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaTddApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaTddApplication.class, args);
	}

}
